package com.tedbin.imageview;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.Block;
import com.google.cloud.vision.v1.Page;
import com.google.cloud.vision.v1.Paragraph;
import com.google.cloud.vision.v1.Symbol;
import com.google.cloud.vision.v1.TextAnnotation;
import com.google.cloud.vision.v1.Word;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.widget.ListPopupWindow.WRAP_CONTENT;
import static com.tedbin.imageview.Constant.MY_PREFS_NAME;
import static com.tedbin.imageview.Constant.canResize;
import static com.tedbin.imageview.Constant.coloredRectsHighlightedSaved;
import static com.tedbin.imageview.Constant.editable;
import static com.tedbin.imageview.Constant.editableIndex;
import static com.tedbin.imageview.Constant.longPressed;
import static com.tedbin.imageview.Constant.needToChange;
import static com.tedbin.imageview.Constant.recIndexNeedChange;
import static com.tedbin.imageview.Constant.rectOnLong;
import static com.tedbin.imageview.Constant.rects;
import static com.tedbin.imageview.Constant.rectsHighLighted;
import static com.tedbin.imageview.Constant.rectsHighlightedSaved;
import static com.tedbin.imageview.Constant.rectsOnLong;
import static com.tedbin.imageview.Constant.rectsToBeHighlighted;
import static com.tedbin.imageview.Constant.symbolRects;
import static com.tedbin.imageview.Constant.temperaryHighlited;
import static com.tedbin.imageview.Constant.touchCurser;


public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;

    ImageView imageView ;
    //CustomImageView imageView ;
    String jsonString ;
    MLine line1,line2 ;
    float scale ;
    View parentView;
    String jString ;
    JSONObject jsonObject;



    // for getting zero for top left corner of imageView
    float x = 0, y = 0 ;
    float xTouch = 0, yTouch = 0 ;
    boolean canHighlight = false ;

    //view
    private final PopupWindow popupWindow = new PopupWindow();
    ImageView highLightRED,highLightYellow,view_ok ;

    private static final int DEFAULT_WIDTH = -1;
    private static final int DEFAULT_HEIGHT = -1;

    private static final int DEFAULT_ANIM_DUR = 350;
    private static final int DEFAULT_ANIM_DELAY = 500;
    private static final int HIGHLIGHT_RED = 1 ;
    private static final int HIGHLIGHT_YELLOW = 2 ;

    private final Point currLoc = new Point();
    private final Point startLoc = new Point();

    private final Rect cbounds = new Rect();
    private final ActionMode.Callback emptyActionMode = new EmptyActionMode();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.img1);

        // setup popup view
        setupPopUpView() ;

        /*Bitmap bitmap = drawRecToBitmap(R.mipmap.sample);
        imageView.setImageBitmap(bitmap);*/
        imageView.setOnTouchListener(touchListener);
        parentView = getWindow().getDecorView().getRootView() ;

        // Instantiate the gesture detector with the
        // application context and an implementation of
        // GestureDetector.OnGestureListener
        mDetector = new GestureDetectorCompat(this,new MyGestureListener());

        mDetector.setIsLongpressEnabled(true);
        jsonString = loadJSONFromAsset();
        getAllRects() ;

        drawSaved(R.mipmap.sample);

        Resources resources = getResources();
        float widthPixels = resources.getDisplayMetrics().widthPixels ;
        float heightPixels = resources.getDisplayMetrics().heightPixels;
        float IntrinsicWidth = imageView.getDrawable().getIntrinsicWidth();
        float IntrinsicHeight = imageView.getDrawable().getIntrinsicHeight();

        scale = IntrinsicWidth/widthPixels ;

        Log.i("accuracy", "scale : " + scale + "  widthPixels : " + widthPixels  + "  heightPixels : " + heightPixels);
        Log.i("accuracy", "scale : " + scale + "  MaxWidth : " + IntrinsicWidth  + "  MaxHeight : " + IntrinsicHeight);

    }



    // This touch listener passes everything on to the gesture detector.
    // That saves us the trouble of interpreting the raw touch events
    // ourselves.
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // pass the events to the gesture detector
            // a return value of true means the detector is handling it
            // a return value of false means the detector didn't
            // recognize the event

            xTouch  =  event.getX() * scale;
            yTouch =  event.getY() ;

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:

                    Rect rect2 = new Rect(rectOnLong.right - 80,rectOnLong.top,rectOnLong.right + 80,rectOnLong.bottom);
                    Rect rect3 = new Rect(rectOnLong.left - 80,rectOnLong.top,rectOnLong.left + 80,rectOnLong.bottom);

                    if (editable){
                        if (rect2.contains(Math.round(xTouch),Math.round(yTouch)) || rect3.contains(Math.round(xTouch),Math.round(yTouch))) {
                            touchCurser = true;

                            for (CustomRect symbolRect : symbolRects) {

                                if (rect2.intersect(symbolRect.rect)) {

                                    recIndexNeedChange = symbolRect.index ;

                                }

                            }


                        }else {
                            popupWindow.dismiss();
                        }


                    } else {
                        popupWindow.dismiss();
                    }



                    break;

                case MotionEvent.ACTION_MOVE :

                    if (touchCurser) {

                        Log.i("Touch", " need to resize" );

                        for (int i = 0;i < symbolRects.size();i++){

                            if (symbolRects.get(i).rect.contains(Math.round(xTouch),Math.round(yTouch))){
                                Constant.recIndexToChange = i ;
                                Log.i("Touch", " resize rec num up to " + i );

                            }

                        }

                        Log.i("Touch", " Constant.recIndexToChange " + Constant.recIndexToChange + " Constant.recIndexNeedChange : " + recIndexNeedChange);


                        rectsToBeHighlighted = new ArrayList<>();

                        if (Constant.recIndexToChange > recIndexNeedChange) {
                            for (int j = recIndexNeedChange ; j < Constant.recIndexToChange + 1; j++){
                                rectsToBeHighlighted.add(symbolRects.get(j).rect);

                            }

                            Bitmap bitmap = drawRecToBitmap(R.mipmap.sample);
                            imageView.setImageBitmap(bitmap);

                        }else {
                            for (int j =  Constant.recIndexToChange ; j < recIndexNeedChange + 1; j++){
                                Constant.rectsToBeHighlighted.add(symbolRects.get(j).rect);

                            }
                            Bitmap bitmap = drawRecToBitmap(R.mipmap.sample);
                            imageView.setImageBitmap(bitmap);
                        }


                        needToChange = false ;
                        longPressed = false ;

                    }

                    break;

                    case MotionEvent.ACTION_UP:
                        Log.i("Touch", " ACTION_UP  : " );



                        break;

            }


            return mDetector.onTouchEvent(event);

        }
    };


    // In the SimpleOnGestureListener subclass you should override
    // onDown and any other gesture that you want to detect.
    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            Log.d("accuracy","onDown: ");

            Log.i("accuracy", "x : " + event.getX() + " y : " + event.getY() );

            // don't return false here or else none of the other
            // gestures will work
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.i("TAG", "onSingleTapConfirmed: ");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

            longPressed = true ;

            Log.i("Touch", "on long press from gesture detector");

            x = (int) e.getX() * scale;
            y = (int) e.getY();

            Log.i("Touch", " y : " + y  + " x : " + x );

            highlightNewWord() ;


        }

        private void highlightNewWord() {

            for (Rect rect : rects) {
                if (rect.contains(Math.round(x),Math.round(y))) {
                    rectOnLong = rect ;

                    editable = true ;
                    editableIndex = rects.indexOf(rect) ;

                    Log.i("TAG", "editableIndex: " + editableIndex);

                }
            }


            Bitmap bitmap = drawOneRecToBitmap(R.mipmap.sample);
            imageView.setImageBitmap(bitmap);
            popupWindow.showAtLocation(imageView, Gravity.TOP,Math.round(x),Math.round(y));

        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.i("TAG", "onDoubleTap: ");
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {

            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            Log.d("TAG", "onFling: ");
            return true;
        }
    }


    public void checkWord (int x,int y) {

        Log.i("Touch", "long edit : x : " + x + " y : " + y);

    }

    public void getAllRects() {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject fullTextAnnotation = jsonObject.getJSONObject("fullTextAnnotation");
            JSONArray pages = fullTextAnnotation.getJSONArray("pages");


            int sym = 0 ;
            for (int p = 0 ; p < pages.length(); p++){

                JSONObject page = pages.getJSONObject(p) ;
                JSONArray blocks = page.getJSONArray("blocks");

                for (int j = 0 ; j < blocks.length(); j++){

                    JSONObject block = blocks.getJSONObject(j) ;
                    JSONArray paragraphs = block.getJSONArray("paragraphs");

                    for (int pr = 0  ; pr < paragraphs.length(); pr++) {

                        JSONObject paragraph = paragraphs.getJSONObject(pr) ;
                        JSONArray words = paragraph.getJSONArray("words");


                        for (int w = 0  ; w < words.length(); w++) {

                            String wordS = "" ;
                            JSONObject word = words.getJSONObject(w) ;
                            JSONArray symbols = word.getJSONArray("symbols");

                            for (int s = 0  ; s < symbols.length(); s++) {


                                JSONObject symbol = symbols.getJSONObject(s) ;

                                String text = symbol.getString("text") ;
                                Log.i("Touch", "text : " + text);

                                JSONObject boundingBox = symbol.getJSONObject("boundingBox");
                                JSONArray vertices = boundingBox.getJSONArray("vertices") ;

                                JSONObject vertice = vertices.getJSONObject(0) ;
                                int left = vertice.getInt("x") ;
                                int top = vertice.getInt("y") ;

                                JSONObject vertice2 = vertices.getJSONObject(1) ;
                                int right = vertice2.getInt("x") ;

                                JSONObject vertice3 = vertices.getJSONObject(2) ;
                                int down = vertice3.getInt("y") ;

                                Rect rect = new Rect(left,top,right,down) ;
                                CustomRect customRect = new CustomRect(rect,sym);
                                Log.i("Touch", "rect symbol: " + rect);

                                symbolRects.add(customRect);

                                sym++;
                            }


                            JSONObject boundingBox = word.getJSONObject("boundingBox");
                            JSONArray vertices = boundingBox.getJSONArray("vertices") ;

                            JSONObject vertice = vertices.getJSONObject(0) ;
                            int left = vertice.getInt("x") ;
                            int top = vertice.getInt("y") ;

                            JSONObject vertice2 = vertices.getJSONObject(1) ;
                            int right = vertice2.getInt("x") ;

                            JSONObject vertice3 = vertices.getJSONObject(2) ;
                            int down = vertice3.getInt("y") ;

                            Rect rect2 = new Rect(left,top,right,down) ;

                            rects.add(rect2);

                        }

                    }

                }

            }


        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("long_text.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public Bitmap drawRecToBitmap (int resId) {

        Resources resources = getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, resId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.RED);
        paint.setAlpha(40);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3f);

        Log.i("Touch", "Constant.rectsToBeHighlighted.size() : " + Constant.rectsToBeHighlighted.size() );

        for (int i = 0; i< Constant.rectsToBeHighlighted.size(); i++) {

            Rect rect = Constant.rectsToBeHighlighted.get(i) ;
            canvas.drawRect(rect,paint);

        }

        canvas.drawRect(rectOnLong,paint);


        if (coloredRectsHighlightedSaved.size() != 0){
            for (ColoredRec coloredRec : coloredRectsHighlightedSaved) {
                if (coloredRec.color == HIGHLIGHT_RED){
                    paint.setColor(Color.RED);
                    paint.setAlpha(40);

                    Constant.color = HIGHLIGHT_RED ;
                } else if (coloredRec.color == HIGHLIGHT_YELLOW) {
                    paint.setColor(getResources().getColor(R.color.yellowHighLight));
                    paint.setAlpha(40);

                    Constant.color = HIGHLIGHT_YELLOW ;
                }
                canvas.drawRect(coloredRec.rect,paint);

            }
        }


        return bitmap;

    }


    public Bitmap drawOneRecToBitmap (int resId) {

        Constant.color = HIGHLIGHT_RED ;

        Resources resources = getResources();
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, resId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.RED);
        paint.setAlpha(40);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3f);

        Paint paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint1.setColor(Color.BLUE);
        paint1.setStyle(Paint.Style.STROKE);
        paint1.setStrokeWidth(10);

        Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint2.setColor(Color.BLUE);
        paint2.setStyle(Paint.Style.FILL);

        canvas.drawRect(rectOnLong,paint);
        canvas.drawLine(rectOnLong.right,rectOnLong.bottom,rectOnLong.right,rectOnLong.top,paint1);
        canvas.drawLine(rectOnLong.left,rectOnLong.bottom,rectOnLong.left,rectOnLong.top,paint1);
        canvas.drawCircle(rectOnLong.right,rectOnLong.bottom,25,paint2);
        canvas.drawCircle(rectOnLong.left,rectOnLong.top,25,paint2);

        ColoredRec c = new ColoredRec(rectOnLong,HIGHLIGHT_RED) ;
        temperaryHighlited.add(c);

        for (ColoredRec coloredRec : coloredRectsHighlightedSaved) {
            if (coloredRec.color == HIGHLIGHT_RED){
                paint.setColor(Color.RED);
                paint.setAlpha(40);

                Constant.color = HIGHLIGHT_RED ;
            } else if (coloredRec.color == HIGHLIGHT_YELLOW) {
                paint.setColor(getResources().getColor(R.color.yellowHighLight));
                paint.setAlpha(40);

                Constant.color = HIGHLIGHT_YELLOW ;
            }
            canvas.drawRect(coloredRec.rect,paint);

        }

        return bitmap;

    }


    public void drawSaved(int resId){

        Resources resources = getResources();
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, resId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(40);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3f);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("rects", null);
        Log.i("ToUCH","restoredText : " + restoredText);
        if (restoredText != null) {
            jsonString = restoredText;
            coloredRectsHighlightedSaved = new Gson().fromJson(jsonString, new TypeToken<ArrayList<ColoredRec>>() {}.getType());

            for (ColoredRec coloredRec : coloredRectsHighlightedSaved) {
                if (coloredRec.color == HIGHLIGHT_RED){
                    paint.setColor(Color.RED);
                    paint.setAlpha(40);

                    //Constant.color = HIGHLIGHT_RED ;
                } else if (coloredRec.color == HIGHLIGHT_YELLOW) {
                    paint.setColor(getResources().getColor(R.color.yellowHighLight));
                    paint.setAlpha(40);
                    //Constant.color = HIGHLIGHT_YELLOW ;
                }
                canvas.drawRect(coloredRec.rect,paint);

            }

        }

        imageView.setImageBitmap(bitmap);
    }

    public void changeColor(int resId,int color){

        Resources resources = getResources();
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, resId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(40);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3f);

        for (ColoredRec coloredRec : temperaryHighlited) {

            if (color == HIGHLIGHT_RED){
                paint.setColor(Color.RED);
                paint.setAlpha(40);
                coloredRec.color = HIGHLIGHT_RED ;

            } else if (color == HIGHLIGHT_YELLOW) {
                paint.setColor(getResources().getColor(R.color.yellowHighLight));
                paint.setAlpha(40);
                coloredRec.color = HIGHLIGHT_YELLOW ;
            }
            canvas.drawRect(coloredRec.rect,paint);

        }

        if (coloredRectsHighlightedSaved.size()!=0 ) {
            for (ColoredRec coloredRec : coloredRectsHighlightedSaved) {

                if (coloredRec.color == HIGHLIGHT_RED){
                    paint.setColor(Color.RED);
                    paint.setAlpha(40);

                } else if (coloredRec.color == HIGHLIGHT_YELLOW) {
                    paint.setColor(getResources().getColor(R.color.yellowHighLight));
                    paint.setAlpha(40);

                }
                canvas.drawRect(coloredRec.rect,paint);
            }
        }
        imageView.setImageBitmap(bitmap);
    }

    public void setupPopUpView() {


        // Initialize the popup content, only add it to the Window once we've selected text
        final LayoutInflater inflater = LayoutInflater.from(this);
        popupWindow.setContentView(inflater.inflate(R.layout.popup, null));
        popupWindow.setWidth(WRAP_CONTENT);
        popupWindow.setHeight(WRAP_CONTENT);

        View popup = popupWindow.getContentView() ;
        highLightRED = popup.findViewById(R.id.view_highlight_red) ;
        highLightYellow = popup.findViewById(R.id.view_highlight_yellow) ;
        view_ok = popup.findViewById(R.id.view_ok) ;

        highLightRED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeColor(R.mipmap.sample,HIGHLIGHT_RED);
            }
        });

        highLightYellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeColor(R.mipmap.sample,HIGHLIGHT_YELLOW);
            }
        });

        view_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                touchCurser = false ;
                editable = false ;
                coloredRectsHighlightedSaved.addAll(temperaryHighlited);

                temperaryHighlited = new ArrayList<>();
                jsonString = new Gson().toJson(coloredRectsHighlightedSaved);

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("rects", jsonString);
                editor.commit();

                drawSaved(R.mipmap.sample);
                popupWindow.dismiss();


            }
        });

    }


    /** An {@link ActionMode.Callback} used to remove all action items from text selection */
    static final class EmptyActionMode extends SimpleActionModeCallback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Return true to ensure the txtBody is still selectable
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // Remove all action items to provide an actionmode-less selection
            menu.clear();
            return true;
        }

    }

}
