package com.tedbin.imageview;

import android.graphics.Rect;

public class ColoredRec {

    public Rect rect ;
    public int color ;

    public ColoredRec (Rect r , int color) {
        this.color = color ;
        this.rect = r;

    }

}
