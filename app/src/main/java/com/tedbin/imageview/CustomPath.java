package com.tedbin.imageview;

import android.graphics.Paint;
import android.graphics.Path;

public class CustomPath {

    Path path;
    Paint paint ;

    public CustomPath (Path path,Paint paint) {
        this.path = path ;
        this.paint = paint ;
    }
}
