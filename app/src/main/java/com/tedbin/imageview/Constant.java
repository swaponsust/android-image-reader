package com.tedbin.imageview;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;

import java.util.ArrayList;

public class Constant {

    public static boolean longPressed = false ;
    public static boolean needToChange = false ;
    public static boolean touchCurser = false ;
    public static boolean canResize = false ;
    public static boolean editable = false ;
    public static int editableIndex = 0 ;
    public static int recIndexNeedChange = 0 ;
    public static int recIndexToChange = 0 ;
    public static int color = 0 ;
    public static int mode = 0 ;

    public static final String MY_PREFS_NAME = "savedRects";

    public static ArrayList<Rect> rects= new ArrayList<>() ;
    public static ArrayList<CustomRect> symbolRects= new ArrayList<>() ;
    public static ArrayList<Rect> rectsToBeHighlighted= new ArrayList<>() ;
    public static ArrayList<Rect> rectsOnLong= new ArrayList<>() ;
    public static Rect rectOnLong= new Rect();
    public static ArrayList<Rect> rectsHighlightedSaved= new ArrayList<>() ;
    public static ArrayList<ColoredRec> coloredRectsHighlightedSaved= new ArrayList<>() ;
    public static ArrayList<ColoredRec> temperaryHighlited= new ArrayList<>() ;
    public static ArrayList<CustomRect> rectsHighLighted= new ArrayList<>() ;
    public static ArrayList<SavedRect> savedRects= new ArrayList<>() ;

    public static Path path = new Path() ;
    public static SavedRect savedRect ;
    public static Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
}
